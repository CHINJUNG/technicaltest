function longestCommonPrefix(strs) {
    if (strs.length === 0) return "";

    let minLength = Number.MAX_SAFE_INTEGER;
    for (let str of strs) {
        minLength = Math.min(minLength, str.length);
    }

    let prefix = "";
    for (let i = 0; i < minLength; i++) {
        let char = strs[0][i];
        for (let j = 1; j < strs.length; j++) {
            if (strs[j][i] !== char) {
                return prefix;
            }
        }
        prefix += char;
    }
    
    return prefix;
}

const stringsOne = ["flower","flow","flight"];
const stringsTwo = ["dog","racecar","car"];
console.log('stringsOne => ' + longestCommonPrefix(stringsOne));
console.log('stringsTwo => ' + longestCommonPrefix(stringsTwo));
